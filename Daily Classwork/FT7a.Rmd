---
title: "Classwork Assignment FT.7a"
output: html_notebook
---

```{r}
#Required Packages
library(forecast);
library(tseries);
```

#Continue our Exploration of models for AirPassengers [Code Copied from FT6]
```{r}
# Setup
nValid <- 12
nTrain <- length(AirPassengers) - nValid
train.ts <- window(AirPassengers, start= c(1949, 1), end = c(1949, nTrain))
valid.ts <- window(AirPassengers, start = c(1949, nTrain+1), end = c(1949, nTrain + nValid))
```

```{r}
# Linear model
AirPassengers.lm <- tslm(train.ts ~ trend, lambda = 1)
AirPassengers.lm.pred <- forecast(AirPassengers.lm, h = nValid, level = 0)

# Exponential
AirPassengers.exp <-tslm(train.ts ~ trend, lambda = 0)
AirPassengers.exp.pred <- forecast(AirPassengers.exp, h = nValid, level=0)

# Polynomial Model Degree 2
AirPassengers.poly2 <- tslm(train.ts ~ trend + I(trend^2))
AirPassengers.poly2.pred <- forecast(AirPassengers.poly2, h = nValid, level = 0)

# Polynomial Model Degree 3
AirPassengers.poly3 <- tslm(train.ts ~ trend + I(trend^2) + I(trend^3))
AirPassengers.poly3.pred <- forecast(AirPassengers.poly2, h = nValid, level = 0)

# Plot of Exponential
plot(AirPassengers.exp.pred, ylab = "Passengers (in Thousands)", xlab = "Time", bty = "l", xaxt = "n")
axis(1, at = seq(1949, 1960, 1), labels = format(seq(1949, 1960, 1)))
lines(valid.ts, lty = 2)

# Exponential Prediction Line
lines(AirPassengers.exp.pred$fitted, lwd = 2, col = "blue")

# Linear
lines(AirPassengers.lm.pred$fitted, col = "red")
lines(AirPassengers.lm.pred$mean, col = "red")

# Polynomials
lines(AirPassengers.poly2.pred$fitted, col = "#008000")
lines(AirPassengers.poly2.pred$mean, col = "#B22222")

lines(AirPassengers.poly3.pred$fitted, col = "#B22222")
lines(AirPassengers.poly3.pred$mean, col = "#B22222")
```

# New Stuff...
## Use the "Acf" function in the "forecast" package to examin autocorrelations for lags 1-12 in the training data set

```{r}
Acf(train.ts, lag.max = 12, main = "")
Acf(train.ts, lag.max = 48, main = "")
```

```
We can see that we have a lot of high positive correlation.  We can look at more lags then we originally intended to see that we have a long term seasonality and trend.
```

```{r}
Acf(AirPassengers.poly3.pred$residuals, lag.max = 12, main = "")
Acf(AirPassengers.poly3.pred$residuals, lag.max = 48, main = "")
```

```
We can even see from the AutoCorrelations from our 3rd Degree polynomial model that we have a trend and seasonality.
However, one could argue that our regression model has not adequately captured all of the trend and seasonality in our timeseries.  (We can see this because we have a decreasing trend and a high negative correlation)
```
#### But what if I add seasonality?
```{r}
# Polynomial Model Degree 3
AirPassengers.poly3.s <- tslm(train.ts ~ trend + I(trend^2) + I(trend^3) + season)
AirPassengers.poly3.s.pred <- forecast(AirPassengers.poly2, h = nValid, level = 0)
Acf(AirPassengers.poly3.s.pred$residuals, lag.max = 12, main = "")
Acf(AirPassengers.poly3.s.pred$residuals, lag.max = 48, main = "")
```

##Use the "Arima" function to perform construct an AR(1) model for the residuals from your favorite regression model.  The code found on page 150 of your text may be helpful here.

```{r}
poly3.res.arima <- Arima(AirPassengers.poly3.s$residuals, order = c(1,0,0))
poly3.res.arima.pred <- forecast(poly3.res.arima, h= nValid)

plot(AirPassengers.poly3.s$residuals, ylab = "Residuals")
lines(poly3.res.arima.pred$fitted, lwd = 2, col = "#20933d")

summary(poly3.res.arima)
```

