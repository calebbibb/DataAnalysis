---
title: "Classwork Assignment FT.5b"
output: html_notebook
---

```{r}
library(forecast)
library(zoo)
```

###Your textbook indicates that the ets package can produce 18 different exponential models (see the table on page 99).  Your single task in this classwork assignment is to split the AirPassenger data into a training and validation set, as done before, and determine which of the 18 models produces the most accurate predictions for the validation set.  You should determine this using both plots and accuracy measures.
```{r}
Dataset <- AirPassengers
nValid <- 12
nTrain <- length(AirPassengers) - nValid
train.ts <- window(AirPassengers, start= c(1949, 1), end = c(1949, nTrain))
valid.ts <- window(AirPassengers, start = c(1949, nTrain+1), end = c(1949, nTrain + nValid))

hwin <- ets(train.ts, model="MAA")
hwin.pred <- forecast(hwin, h = nValid)
```

