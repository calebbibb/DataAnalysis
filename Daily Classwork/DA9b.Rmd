---
title: "Classwork Assignment DA.9b"
output: html_notebook
---

###Let's Setup our Dataset:
```{r}
df <- read.csv("https://ed-public-download.apps.cloud.gov/downloads/Most-Recent-Cohorts-Scorecard-Elements.csv",stringsAsFactors=F);
my.df <- data.frame("PREDDEG"=cbind(as.character(df$PREDDEG)));
my.df$UGDS <- as.numeric(df$UGDS);
my.df$CPTR <- as.numeric(df$PCIP11);
my.df$MATH <- as.numeric(df$PCIP30);
my.df$BUSN <- as.numeric(df$PCIP52);
my.df$CPTR <- ifelse(is.na(my.df$CPTR),0,my.df$CPTR);
my.df$MATH <- ifelse(is.na(my.df$MATH),0,my.df$MATH);
my.df$BUSN <- ifelse(is.na(my.df$BUSN),0,my.df$BUSN);
my.df <- my.df[!is.na(my.df$UGDS),];
```
###Next, split your data (there should be 6990 rows in the my.df data frame) into a training and a testing set.  Put 2/3rds of your data in the training set and the remaining 1/3 in the testing set.
```{r}
6990*.66
train <- sample(1:nrow(my.df), 4613);
training <- my.df[train,];
testing <- my.df[-train,];
```

###Now install and load the tree package and create a decision tree for PREDDEG based on the other variables in the training set.  What is the accuracy of your decision tree on the training set?
```{r}
library(tree);
my.tree <- tree(PREDDEG ~ ., data=training);
summary(my.tree);
```
> The accuracy of my decision tree on the error set is unfortunately, 69%.

###Plot your tree and describe, in your own words, how the classification decision is made.
```{r}
plot(my.tree)
text(my.tree)
```
>We can see that the leaf on the left does not have a BUSN<.02705 and so on. The Left leaves being those that do not complete the requirement.  These conditions were chosen by the decision tree generating algorithm to find the best distinctions.

###Prune your tree down to four leaves and repeat parts (3), (4), (5) above.
```{r}
set.seed(3)
tree.results <- cv.tree(my.tree, FUN=prune.misclass)
plot(tree.results$size, tree.results$dev, type="b")
```
> We can see that 7 is the optimal number of nodes, however for this assignment we're going to prune anyway.

```{r}
pruned.tree <- prune.misclass(my.tree, best = 3)
plot(pruned.tree)
text(pruned.tree)
summary(pruned.tree)
```
> We can see that our error rate is 33% which means we have 66% accuracy.

###Now create a random forest based on your training data. Use ntree = 1000 and mtry = 4 and be prepared to wait a while.
```{r}
#First the SETUP!
#install.packages(c("class", "mlbench", "chemometrics"))
library(class)
library(mlbench)
library(randomForest)
#predictions <- knn(scale(training[,-9]),
 #                  scale(testing[,-9]),
  #                 training[,9], k=27)
accuracy <- function(predictions, answers){
  sum((predictions==answers)/(length(answers)))
}
```


```{r}
library(randomForest)
forest<- randomForest(PREDDEG ~ ., data=training,
                      importance= TRUE, 
                      ntree = 1000,
                      mtry = 4)
```

```{r}
#This doesn't work. Consult Data Analysis with R Ch9
accuracy(predict(forest), training[,9])
```

